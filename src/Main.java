import automata.DFA;
import automata.NFA;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Program Loop
        int command;
        while (true) {
            System.out.println("1. Enter New NFA");
            System.out.println("2. Exit");
            command = scanner.nextInt();

            if (command == 1) createNFA();
            else break;
        }
    }

    // Start a process (NFA -> DFA -> Minimal DFA)
    public static void createNFA() {
        Scanner scanner = new Scanner(System.in);

        NFA nfa = new NFA();

        // Get DFA's Alphabet
        System.out.println("Enter NFA's alphabet, do not separate");
        String alphabet = scanner.nextLine();
        nfa.setAlphabet(alphabet.toCharArray());

        // Get DFA's States
        System.out.println("Enter NFA's states, separate by space");
        String states = scanner.nextLine();
        nfa.setStates(states.split(" "));

        // Get DFA's Start State
        System.out.println("Enter NFA's start state");
        String S = scanner.nextLine();
        nfa.setStartState(S);

        // Get DFA's Final States
        System.out.println("Enter NFA's final states, separate by space");
        String finalStates = scanner.nextLine();
        nfa.setFinalStates(finalStates.split(" "));

        // Get DFA's Transitions
        System.out.println("Enter NFA's transitions like q0 a q1 (use ! for lambda), enter -1 when finished");
        while (true) {
            String input = scanner.nextLine();
            try {
                int command = Integer.parseInt(input);
                break;
            } catch (Exception e) {
                String[] parts = input.split(" ");
                nfa.addTransitionByName(parts[0], parts[1].charAt(0), parts[2]);
            }
        }

        // NFA -> DFA
        DFA dfa = nfa.toDFA();
        dfa.print("DFA");

        // Normalize Weird State Names
        dfa.normalizeNames();

        // DFA -> Minimal DFA
        DFA minimalDFA = dfa.toMinimalDFA();
        minimalDFA.print("Minimal DFA");

        // Test DFA
        System.out.println("Enter Strings, enter -1 when finished");
        while (true) {
            String input = scanner.nextLine();
            try {
                int command = Integer.parseInt(input);
                break;
            } catch (Exception e) {
                if (dfa.accepts(input)) System.out.println("Accepted");
                else System.out.println("Rejected");
            }
        }
    }
}
