package automata;

import java.util.ArrayList;

public class Automaton {
    private char[] alphabet;
    private ArrayList<State> states = new ArrayList<>();
    private State S;

    // Set Alphabet Directly
    public void setAlphabet(char[] alphabet) {
        this.alphabet = alphabet;
    }

    // Set States by Name
    public void setStates(String[] stateNames) {
        for (String stateName: stateNames) {
            states.add(new State(stateName));
        }
    }

    // Add a New State by name
    public State addState(String stateName) {
        State s = new State(stateName);
        states.add(s);
        return s;
    }

    // Set Start State by Name
    public void setStartState(String stateName) {
        for (State s : states) {
            if (s.getName().equals(stateName)) {
                S = s;
                break;
            }
        }
    }

    // Set Final States by Name
    public void setFinalStates(String[] stateNames) {
        for (String name : stateNames) {
            for (State s : states) {
                if (s.getName().equals(name)) {
                    s.setFinal(true);
                    break;
                }
            }
        }
    }

    // Add a New Final State by Name
    public void addFinalState(String name) {
        for (State s : states) {
            if (s.getName().equals(name)) {
                s.setFinal(true);
                break;
            }
        }
    }

    public char[] getAlphabet() {
        return alphabet;
    }

    public ArrayList<State> getStates() {
        return states;
    }

    public ArrayList<State> getFinalStates() {
        ArrayList<State> finals = new ArrayList<>();

        for (State s : states) {
            if (s.isFinal()) finals.add(s);
        }

        return finals;
    }

    public State getS() {
        return S;
    }

    public State getStateByName(String name) {
        for (State s : states) {
            if (s.getName().equals(name)) return s;
        }
        return null;
    }

    public State getStateByPartialName(String name) {
        for (State s : states) {
            if (s.getName().contains(name)) return s;
        }
        return null;
    }

    // Print Shared Properties of Automata
    public void printGeneral() {
        System.out.print("Σ = { ");
        for (char a : alphabet) System.out.print(a + " ");
        System.out.println("}");

        System.out.print("Q = { ");
        for (State s : states) System.out.print(s.getName() + " ");
        System.out.println("}");

        System.out.println("S = { " + S.getName() + " }");

        System.out.print("F = { ");
        for (State s : states) if (s.isFinal()) System.out.print(s.getName() + " ");
        System.out.println("}");
    }

    public void printStar() {
        for (int i = 0; i < 100; i++) System.out.print("*");
        System.out.println();
        for (int i = 0; i < 100; i++) System.out.print("*");
        System.out.println();
    }
}
