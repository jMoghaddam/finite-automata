package automata;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class DFA extends Automaton {
    private HashMap<String, State> transitions = new HashMap<>();

    public void addTransition(State src, char alphabet, State dest) {
        String key = src.getName() + alphabet;
        transitions.put(key, dest);
    }

    public State transit(State src, char alphabet) {
        return transitions.get(src.getName() + alphabet);
    }

    public void normalizeNames() {
        HashMap<String, State> newTransitions = new HashMap<>();
        int i = 0;
        for (State s : getStates()) {
            transitions.forEach((k, v) -> {
                String srcName = k.substring(0, k.length() - 1);
                if (srcName.equals(s.getName())) {
                    State src = getStateByName(srcName);
                    char a = k.charAt(k.length() - 1);
                    newTransitions.put("q" + getStates().indexOf(src) + a, v);
                }
            });

            s.setName("q" + i);
            i++;
        }

        transitions = newTransitions;
    }

    public DFA toMinimalDFA() {
        // Create a Copy of NFA
        DFA copy = new DFA();

        copy.setAlphabet(getAlphabet());
        for (State s : getStates()) {
            copy.addState(s.getName());
            if (s.isFinal()) copy.addFinalState(s.getName());
        }
        copy.setStartState(getS().getName());
        transitions.forEach((k, v) -> {
            String srcName = k.substring(0, k.length() - 1);
            State src = copy.getStateByName(srcName);
            char alphabet = k.charAt(k.length() - 1);
            State dest = copy.getStateByName(v.getName());
            copy.addTransition(src, alphabet, dest);
        });

        // Step 1 of Mark Procedure
        boolean[] visited = new boolean[getStates().size()];
        Queue<State> q = new LinkedList<>();
        visited[copy.getStates().indexOf(copy.getS())] = true;
        q.add(copy.getS());

        while (!q.isEmpty()) {
            State s = q.remove();
            for (char a : copy.getAlphabet()) {
                State dest = copy.transit(s, a);
                if (!visited[copy.getStates().indexOf(dest)]) {
                    visited[copy.getStates().indexOf(dest)] = true;
                    q.add(dest);
                }
            }
        }

        for (int i = 0; i < visited.length; i++) {
            if (!visited[i]) {
                copy.getStates().remove(i);
            }
        }

        // Step 2 of Mark Procedure
        int statesSize = copy.getStates().size();
        boolean[][] pairsDistinguish = new boolean[statesSize][statesSize];

        for (int i = 0; i < pairsDistinguish.length; i++) {
            for (int j = 0; j < pairsDistinguish.length; j++) {
                pairsDistinguish[i][j] = false;
            }
        }

        for (int i = 0; i < statesSize; i++) {
            for (int j = i; j < statesSize; j++) {
                State s1 = copy.getStates().get(i);
                State s2 = copy.getStates().get(j);
                if ((s1.isFinal() && !s2.isFinal()) || (!s1.isFinal() && s2.isFinal())) {
                    pairsDistinguish[i][j] = true;
                    pairsDistinguish[j][i] = true;
                }
            }
        }

        // Step 3 of Mark Procedure
        boolean loop = true;
        while (loop) {
            boolean markedThisRound = false;
            for (int i = 0; i < statesSize; i++) {
                for (int j = 0; j < statesSize; j++) {
                    if (!pairsDistinguish[i][j]) {
                        State s1 = copy.getStates().get(i);
                        State s2 = copy.getStates().get(j);

                        for (char a : copy.getAlphabet()) {
                            State dest1 = copy.transit(s1, a);
                            State dest2 = copy.transit(s2, a);

                            if (pairsDistinguish[copy.getStates().indexOf(dest1)][copy.getStates().indexOf(dest2)]) {
                                pairsDistinguish[copy.getStates().indexOf(s1)][copy.getStates().indexOf(s2)] = true;
                                markedThisRound = true;
                            }
                        }

                    }
                }
            }
            loop = markedThisRound;
        }

        // Set Shared Properties for Minimal DFA
        DFA minimalDFA = new DFA();
        minimalDFA.setAlphabet(getAlphabet());

        // Step 2 of Reduce Procedure
        boolean[] grouped = new boolean[statesSize];
        for (int k = 0; k < statesSize; k++) {
            if (!grouped[k]) {
                grouped[k] = true;
                StringBuilder sb = new StringBuilder();
                sb.append("{ ").append(copy.getStates().get(k).getName()).append(" ");

                for (int l = k; l < statesSize; l++) {
                    if (l != k && !pairsDistinguish[k][l]) {
                        grouped[l] = true;
                        sb.append(copy.getStates().get(l).getName()).append(" ");
                    }
                }

                sb.append("}");
                String stateName = sb.toString();
                minimalDFA.addState(stateName);
            }
        }

        // Step 3 of Mark Procedure
        copy.transitions.forEach((k, v) -> {
            String srcName = k.substring(0, k.length() - 1);
            char a = k.charAt(k.length() - 1);
            String destName = v.getName();

            State src = minimalDFA.getStateByPartialName(srcName);
            State dest = minimalDFA.getStateByPartialName(destName);

            minimalDFA.addTransition(src, a, dest);
        });

        // Step 4 of Mark Procedure
        State S = minimalDFA.getStateByPartialName(copy.getS().getName());
        minimalDFA.setStartState(S.getName());

        // Step 5 of Mark Procedure
        for (State s : copy.getStates()) {
            if (s.isFinal()) {
                State minimalState = minimalDFA.getStateByPartialName(s.getName());
                minimalDFA.addFinalState(minimalState.getName());
            }
        }

        return minimalDFA;
    }

    public boolean accepts(String input) {
        State current = getS();
        for (char c : input.toCharArray()) {
            current = transit(current, c);
        }
        return current.isFinal();
    }

    public void print(String title) {
        printStar();

        System.out.println(title);
        printGeneral();

        for (State s : getStates()) {
            for (char a : getAlphabet()) {
                System.out.println("δ(" + s.getName() + ", " + a + ") = " + transitions.get(s.getName() + a).getName());
            }
        }

        printStar();
    }
}
