package automata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class NFA extends Automaton {
    // Store a Transition like (q0, a) => q1, as "q0a":"q1" in a Map
    private HashMap<String, ArrayList<State>> transitions = new HashMap<>();

    public void addTransitionByName(String srcName, char alphabet, String destName) {
        State src = getStateByName(srcName);
        State dest = getStateByName(destName);
        addTransition(src, alphabet, dest);
    }

    public void addTransition(State src, char alphabet, State dest) {
        String key = src.getName() + alphabet;
        ArrayList<State> currentStates = transitions.get(key);

        if (currentStates == null) {
            currentStates = new ArrayList<>();
            currentStates.add(dest);
            transitions.put(key, currentStates);
        } else currentStates.add(dest);
    }

    // **The Transit Function**
    public ArrayList<State> transit(State src, char alphabet) {
        return transitions.get(src.getName() + alphabet);
    }

    // **The Star-Closure Transit Function**
    public ArrayList<State> transitStar(State src, char alphabet) {
        ArrayList<State> states = new ArrayList<State>();
        Queue<State> lambdaLeftFree = new LinkedList<>();
        Queue<State> lambdaFull = new LinkedList<>();

        ArrayList<State> startStatesByA = transit(src, alphabet);
        if (startStatesByA != null) lambdaLeftFree.addAll(startStatesByA);

        ArrayList<State> startStatesByL = transit(src, '!');
        if (startStatesByL != null) lambdaFull.addAll(startStatesByL);

        while (!lambdaFull.isEmpty()) {
            State s = lambdaFull.remove();

            startStatesByA = transit(s, alphabet);
            if (startStatesByA != null) lambdaLeftFree.addAll(startStatesByA);

            startStatesByL = transit(s, '!');
            if (startStatesByL != null) lambdaFull.addAll(startStatesByL);
        }

        while (!lambdaLeftFree.isEmpty()) {
            State s = lambdaLeftFree.remove();
            if (!states.contains(s)) states.add(s);
            ArrayList<State> dests = transit(s, '!');
            if (dests != null) lambdaLeftFree.addAll(dests);
        }

        return states;
    }

    // Does this NFA Accept Empty String?
    public boolean acceptsLambda() {
        Queue<State> q = new LinkedList<>();
        q.add(getS());

        while (!q.isEmpty()) {
            State s = q.remove();
            if (s.isFinal()) return true;
            ArrayList<State> dests = transit(s, '!');
            if (dests != null) q.addAll(dests);
        }

        return false;
    }

    public DFA toDFA() {

        // Set Shared Properties for DFA
        DFA dfa = new DFA();
        dfa.setAlphabet(this.getAlphabet());

        // Step 1 of NFA-to-DFA Procedure
        State S = dfa.addState("{ " + getS().getName() + " }");
        dfa.setStartState("{ " + getS().getName() + " }");

        // Step 2 of NFA-to-DFA Procedure (with the Help of Queue)
        Queue<State> q = new LinkedList<>();
        q.add(S);

        while (!q.isEmpty()) {
            State s = q.remove();
            String[] parts = s.getName().split(" ");
            String[] statesNames = new String[parts.length - 2];
            for (int i = 1; i < parts.length - 1; i++) {
                statesNames[i - 1] = parts[i];
            }

            ArrayList<State> dests = new ArrayList<>();
            for (char a : getAlphabet()) {
                dests.clear();
                for (String name : statesNames) {
                    State src = getStateByName(name);
                    ArrayList<State> destinations = transitStar(src, a);
                    if (destinations != null) {
                        for (State tempS : destinations) {
                            if (!dests.contains(tempS)) dests.add(tempS);
                        }
                    }
                }

                StringBuilder stateNameBuilder = new StringBuilder();
                stateNameBuilder.append("{ ");
                for (State dest : dests) {
                    stateNameBuilder.append(dest.getName()).append(" ");
                }
                stateNameBuilder.append("}");

                String stateName = stateNameBuilder.toString();
                State dfaState = dfa.getStateByName(stateName);
                if (dfaState == null) {
                    dfaState = dfa.addState(stateName);
                    q.add(dfaState);
                }

                dfa.addTransition(s, a, dfaState);
            }
        }

        // Step 3 of NFA-to-DFA Procedure
        ArrayList<State> dfaStates = dfa.getStates();
        ArrayList<State> nfaFinals = getFinalStates();

        for (State state : dfaStates) {
            for (State f : nfaFinals) {
                if (state.getName().contains(f.getName())) dfa.addFinalState(state.getName());
            }
        }

        // Step 4 of NFA-to-DFA Procedure
        if (acceptsLambda()) dfa.addFinalState(S.getName());

        return dfa;
    }

    public void print() {
        printStar();

        System.out.println("NFA");
        printGeneral();

        for (State s : getStates()) {
            for (char a : getAlphabet()) {
                ArrayList<State> dests = transitions.get(s.getName() + a);
                if (dests != null) {
                    for (State dest : dests) {
                        System.out.println("δ(" + s.getName() + ", " + a + ") = " + dest.getName());
                    }
                }
            }
        }

        printStar();
    }
}
